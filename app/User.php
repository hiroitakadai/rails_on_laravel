<?php

namespace App;

use App\Micropost;
use App\Mail\AccountActivation;
use App\Mail\PasswordReset;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use FormAccessible;

    protected $table = 'users';

    protected $guarded = ["id", "admin"];

    public function microposts()
    {
        return $this->hasMany("App\Micropost");
    }

    public function activeRelationships()
    {
        return $this->hasMany("App\Relationship", "follower_id");
    }

    public function passiveRelationships()
    {
        return $this->hasMany("App\Relationship", "followed_id");
    }

    public function following()
    {
        return $this->hasManyThrough("App\User", "App\Relationship", "follower_id", "id", "id", "followed_id");
    }

    public function followers()
    {
        return $this->hasManyThrough("App\User", "App\Relationship", "followed_id", "id", "id", "follower_id");
    }
    
    public function follow($user_id)
    {
        $this->activeRelationships()->create(["followed_id" => $user_id]);
    }

    public function unfollow($user_id)
    {
        $this->activeRelationships()->where("followed_id", $user_id)->delete();
    }

    public function isFollowing($user_id)
    {
        return (bool) Relationship::where("follower_id", $this->id)->where("followed_id", $user_id)->count();
    }

    public function isFollowed($user_id)
    {

        return (bool) Relationship::where("follower_id", $user_id)->where("followed_id", $this->id)->count();
    }

    public function feed()
    {
        $relations = $this->activeRelationships()->get()->toArray();
        $followed_ids = array_pluck($relations, "followed_id");
        return Micropost::whereIn("user_id", $followed_ids)->orWhere("user_id", $this->id);
    }

    public function activate()
    {
        $this->activated = true;
        $this->activated_at = Carbon::now();
        $this->save();
    }

    public function sendActivateEmail($token)
    {
        Mail::to($this)->send(new AccountActivation($this, $token));
    }

    public function sendPasswordResetMail($token)
    {
        Mail::to($this)->send(new PasswordReset($this, $token));
    }

    public function checkExpiration()
    {
        return $this->reset_sent_at < Carbon::now()->subHours(2);
    }
}
