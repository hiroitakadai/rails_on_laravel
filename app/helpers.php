<?php

if (! function_exists('full_title')) {

    function full_title($page_title = '')
    {
        $base_title = "Ruby on Rails Tutorial Sample App";
        if (empty($page_title)) {
            return $base_title;
        } else {
            return $page_title . " | " . $base_title;
        }
    }
    
}

if (! function_exists('gravatar_for')) {
    function gravatar_for($user, $options = ["size" => 80])
    {
        $gravatar_id = md5(strtolower($user->email));
        $gravatar_url = "https://secure.gravatar.com/avatar/{$gravatar_id}?s={$options["size"]}";
        return Html::image($gravatar_url, $user->name, ["class" => "gravatar"]);
    }
}

if (! function_exists('time_ago_in_words')) {
    function time_ago_in_words($date)
    {
        return \Carbon\Carbon::parse($date)->diffForHumans();
    }
}
