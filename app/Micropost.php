<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Micropost extends Model
{
    protected $guarded = ["id"];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('created_at', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });
    }

    public function user()
    {
        return $this->belongsTo("App\User");
    }
}
