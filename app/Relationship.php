<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relationship extends Model
{
    protected $guarded = ['id'];

    public function follower()
    {
        return $this->belongsTo("App\User", "id", "follower_id");
    }

    public function followed()
    {
        return $this->belongsTo("App\User", "id", "followed_id");
    }
}
