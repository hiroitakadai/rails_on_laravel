<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            session()->flash('message', ['danger' => 'Please login.']);
            session(["url.intended" => url()->current()]);
            return redirect('/login');
        }

        return $next($request);
    }
}
