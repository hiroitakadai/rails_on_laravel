<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountActivationsController extends Controller
{
    public function edit(Request $request, $token)
    {
        $user = User::where('email', $request->email)->first();
        if ($user && !$user->activated && Hash::check($token, $user->activation_digest)) {
            $user->activate();
            Auth::login($user);
            session()->flash('message', ['success' => 'Account activated!']);
            return redirect()->route("users.show", $user->id);
        } else {
            session()->flash('message', ['danger' => 'Invalid activation link']);
            return redirect("/");
        }
    }
}
