<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SessionsController extends Controller
{
    public function create()
    {
        return view("sessions.create");
    }

    public function store(Request $request)
    {
        $user = User::where("email", strtolower($request->email))->first();
        if ($user && Hash::check($request->password, $user->password)) {
            if ($user->activated === true) {
                Auth::login($user, $request->remember_me === "1");
                return redirect()->intended(route("users.show", $user->id));
            } else {
                session()->flash('message', ['warning' => 'Account not activated. Check your email for the activation link.']);
                return redirect("/");
            }
        } else {
            session()->flash('message', ['danger' => 'Invalid email/password combination']);
            return back()->withInput();
        }
    }

    public function destroy()
    {
        Auth::logout();
        return redirect("/");
    }
}
