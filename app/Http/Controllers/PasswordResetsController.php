<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class PasswordResetsController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $user = User::where("email", $request->email)->first();
            if (!$user || !$user->activated || !Hash::check($request->token, $user->reset_digest)) {
                return redirect('/');
            }
            return $next($request);
        })->only(["edit", "update"]);
        $this->middleware(function ($request, $next) {
            $user = User::where("email", $request->email)->first();
            if ($user->checkExpiration()) {
                session()->flash('message', ['danger' => 'Password reset has expired']);
                return redirect()->back();
            }
            return $next($request);
        })->only(["edit", "update"]);
    }

    public function create()
    {
        return view("password_resets.create");
    }

    public function store(Request $request)
    {
        $user = User::where("email", strtolower($request->email))->first();
        if (!$user) {
            session()->flash('message', ['danger' => 'Email address not found']);
            return redirect()->back();
        }
        $reset_token = str_random(22);
        $user->reset_digest = bcrypt($reset_token);
        $user->reset_sent_at = Carbon::now();
        $user->save();
        $user->sendPasswordResetMail($reset_token);
        session()->flash('message', ['info' => 'Email sent with password reset instructions']);
        return redirect("/");
    }

    public function edit(Request $request)
    {
        $user = User::where("email", $request->email)->first();
        return view("password_resets.edit")->with(["user" => $user, "token" => $request->token]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);

        $user = User::where("email", $request->email)->first();
        $user->password = bcrypt($request->password);
        $user->reset_digest = null;
        $user->save();
        Auth::login($user);
        session()->flash('message', ['success' => 'Password has been reset.']);
        return redirect()->route("users.show", $user->id);
    }
}
