<?php

namespace App\Http\Controllers;

use App\Micropost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class MicropostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('authenticate')->only(["store", "destroy"]);
        $this->middleware(function ($request, $next) {
            $micropost = Auth::user()->microposts()->where("id", $request->micropost);
            if ($micropost->count() === 0) {
                return redirect("/");
            }
            return $next($request);
        })->only(["destroy"]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required|max:140',
            'picture' => 'nullable|mimes:jpeg,gif,png|image|max:5120'
        ]);
        $micropost = new Micropost;
        $micropost->content = $request->content;
        if ($request->hasFile('picture')) {
            $file = $request->picture;
            $path = "micropost_proto/" . $file->hashName();
            $encode_file = Image::make($file)->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->encode();
            Storage::put($path, (string) $encode_file, "public");
            $micropost->picture = $path;
        }
        Auth::user()->microposts()->save($micropost);
        session()->flash('message', ['success' => 'Micropost created!']);
        return redirect("/");
    }

    public function destroy($id)
    {
        Micropost::find($id)->delete();
        session()->flash('message', ['success' => 'Micropost deleted']);
        return back();
    }
}
