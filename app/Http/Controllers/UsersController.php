<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('authenticate')->only(["index", "edit", "update", "destroy", "following", "followers"]);

        $this->middleware(function ($request, $next) {
            if ($request->user != Auth::id()) {
                return redirect('/');
            }
            return $next($request);
        })->only(["edit", "update"]);

        $this->middleware(function ($request, $next) {
            if (Auth::user()->admin === false) {
                return redirect('/');
            }
            return $next($request);
        })->only(["destroy"]);
    }

    public function index()
    {
        $users = User::where("activated", true)->paginate(30);
        return view("users.index")->with("users", $users);
    }

    public function create()
    {
        return view("users.create");
    }

    public function store(Request $request)
    {
        $request->email = strtolower($request->email);
        $request->validate([
            "name" => "required|max:50",
            "email" => "required|max:255|email|unique:users",
            "password" => "required|confirmed|min:6",
            "password_confirmation" => "required|min:6"
        ]);
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $activation_token = str_random(22);
        $user->activation_digest = bcrypt($activation_token);
        $user->save();
        $user->sendActivateEmail($activation_token);
        session()->flash('message', ['info' => 'Please check your email to activate your account.']);
        return redirect("/");
    }

    public function show($id)
    {
        $user = User::find($id);
        if ($user->activated) {
            $microposts = $user->microposts()->paginate(30);
            return view('users.show')->with(['user' => $user, 'microposts' => $microposts]);
        } else {
            return redirect("/");
        }
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit')->with('user', $user);
    }

    public function update(Request $request, $id)
    {
        $request->email = strtolower($request->email);
        $request->validate([
            "name" => "required|max:50",
            "email" => ['required', 'max:255', 'email', Rule::unique('users')->ignore(Auth::id())],
            "password" => "nullable|confirmed|min:6",
            "password_confirmation" => "nullable|min:6"
        ]);
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->save();
        session()->flash("message", ['success' => 'Profile updated']);
        return redirect()->route("users.show", $user->id);
    }

    public function destroy($id)
    {
        User::destroy($id);
        session()->flash("message", ["success" => "User deleted"]);
        return redirect()->route("users.index");
    }

    public function following($user)
    {
        $title = "Following";
        $user  = User::find($user);
        $users = $user->following()->paginate(30);
        return view("users.show_follow")->with(["title" => $title, "user" => $user, "users" => $users]);
    }

    public function followers($user)
    {
        $title = "Following";
        $user  = User::find($user);
        $users = $user->followers()->paginate(30);
        return view("users.show_follow")->with(["title" => $title, "user" => $user, "users" => $users]);
    }
}
