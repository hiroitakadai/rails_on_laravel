<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'StaticPagesController@home');
Route::get('help', 'StaticPagesController@help')->name('help');
Route::get('about', 'StaticPagesController@about')->name('about');
Route::get('contact', 'StaticPagesController@contact')->name('contact');

Route::get('signup', 'UsersController@create')->name('signup');
Route::post('signup', 'UsersController@store');
Route::resource('users', "UsersController", ["except" => ["create", "store"]]);

Route::prefix('users')->group(function () {
    Route::get('{user}/following', "UsersController@following")->name("following");
    Route::get('{user}/followers', "UsersController@followers")->name("followers");
});

Route::get('login', "SessionsController@create")->name('login');
Route::post('login', "SessionsController@store");
Route::delete('logout', "SessionsController@destroy")->name('logout');

Route::get('account_activations/{token}/edit', "AccountActivationsController@edit")->name('activation');

Route::get('password_resets/create', "PasswordResetsController@create")->name("resets.create");
Route::post('password_resets', "PasswordResetsController@store")->name("resets.store");
Route::get('password_resets/{token}/edit', "PasswordResetsController@edit")->name("resets.edit");
Route::patch('password_resets/{token}', "PasswordResetsController@update")->name("resets.update");

Route::resource('microposts', "MicropostsController", ["only"=> ['store', 'destroy']]);

Route::resource('relationships', "RelationshipsController", ["only" => ['store', 'destroy']]);
