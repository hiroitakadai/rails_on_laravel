<?php

use App\Micropost;
use Faker\Generator as Faker;

$factory->define(Micropost::class, function (Faker $faker) {
    return [
        'content' => $faker->text,
        'created_at' => $faker->dateTimeThisYear,
    ];
});
