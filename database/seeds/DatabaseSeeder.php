<?php

use App\User;
use App\Micropost;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        User::create([
            "name" => "Example User", 
            "email" => "example@railstutorial.org", 
            "password" => bcrypt("foobar"), 
            "admin" => true,
            "activated" => true,
            "activated_at" => Carbon::now()
        ]);

        User::create([
            "name" => "aaaaaa", 
            "email" => "aaaaaa@test.test", 
            "password" => bcrypt("aaaaaa"), 
            "activated" => true,
            "activated_at" => Carbon::now()
        ]);

        User::create([
            "name" => "bbbbbb", 
            "email" => "bbbbbb@test.test", 
            "password" => bcrypt("bbbbbb"), 
            "activated" => true,
            "activated_at" => Carbon::now()
        ]);

        User::create([
            "name" => "cccccc", 
            "email" => "cccccc@test.test", 
            "password" => bcrypt("cccccc"), 
            "activated" => true,
            "activated_at" => Carbon::now()
        ]);

        factory(User::class, 99)->create();

        User::take(6)->get()->each(function ($u) {
            $u->microposts()->saveMany(Factory(Micropost::class, 50)->make(["user_id" => $u["id"]]));
        });

        $user = User::first();
        $following = User::where("id",  ">=", 2)->where("id", "<=", 50);
        $followers = User::where("id",  ">=", 3)->where("id", "<=", 40);
        $following->each(function ($following) use ($user) { $user->follow($following->id); });
        $followers->each(function ($follower)  use ($user) { $follower->follow($user->id);  });
    }
}
