<?php

use App\User;
use App\Micropost;
use App\Relationship;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class TestSeeder extends Seeder
{
    public function run()
    {
        User::create(["name" => "Michael Example", "email" => "michael@example.com", "password" => bcrypt("password"), "admin" => true, "activated" => true, "activated_at" => Carbon::now()]);
        User::create(["name" => "Sterling Archer", "email" => "duchess@example.gov", "password" => bcrypt("password"), "activated" => true, "activated_at" => Carbon::now()]);
        User::create(["name" => "Lana Kane",       "email" => "hands@example.gov",   "password" => bcrypt("password"), "activated" => true, "activated_at" => Carbon::now()]);
        User::create(["name" => "Malory Archer",   "email" => "boss@example.gov",    "password" => bcrypt("password"), "activated" => true, "activated_at" => Carbon::now()]);

        factory(User::class, 30)->create();

        Micropost::create([
            "content" => "I just ate an orange!",
            "created_at" => Carbon::now()->subminutes(10),
            "user_id" => 1
        ]);
        Micropost::create([
            "content" => "Check out the @tauday site by @mhartl: http://tauday.com",
            "created_at" => Carbon::now()->subYears(3),
            "user_id" => 1
        ]);
        Micropost::create([
            "content" => "Sad cats are sad: http://youtu.be/PKffm2uI4dk",
            "created_at" => Carbon::now()->subHours(2),
            "user_id" => 1
        ]);
        Micropost::create([
            "content" => "Writing a short test",
            "created_at" => Carbon::now(),
            "user_id" => 1
        ]);
        Micropost::create([
            "content" => "Oh, is that what you want? Because that's how you get ants!",
            "created_at" => Carbon::now()->subYears(2),
            "user_id" => 2
        ]);
        Micropost::create([
            "content" => "Danger zone!",
            "created_at" => Carbon::now()->subDays(3),
            "user_id" => 2
        ]);
        Micropost::create([
            "content" => "I'm sorry. Your words made sense, but your sarcastic tone did not.",
            "created_at" => Carbon::now()->subMinutes(10),
            "user_id" => 3
        ]);
        Micropost::create([
            "content" => "Dude, this van's, like, rolling probable cause.",
            "created_at" => Carbon::now()->subHours(4),
            "user_id" => 3
        ]);

        factory(Micropost::class, 30)->create(["user_id" => 1, "created_at" => Carbon::now()->subDays(42)]);

        Relationship::create(["follower_id" => 1, "followed_id" => 3]);
        Relationship::create(["follower_id" => 1, "followed_id" => 4]);
        Relationship::create(["follower_id" => 3, "followed_id" => 1]);
        Relationship::create(["follower_id" => 2, "followed_id" => 1]);
    }
}
