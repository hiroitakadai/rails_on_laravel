<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPictureToMicroposts extends Migration
{
    public function up()
    {
        Schema::table('microposts', function (Blueprint $table) {
            $table->string('picture')->nullable();
        });
    }

    public function down()
    {
        Schema::table('microposts', function (Blueprint $table) {
            $table->dropColumn('picture');
        });
    }
}
