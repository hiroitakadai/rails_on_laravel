<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActivationToUsers extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('activation_digest')->nullable();
            $table->boolean('activated')->default(false);
            $table->dateTime('activated_at')->nullable();
        });
    }


    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('activation_digest');
            $table->dropColumn('activated');
            $table->dropColumn('activated_at');
        });
    }
}
