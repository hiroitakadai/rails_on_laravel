<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;

class UsersLoginTest extends TestCase
{
    private $user;
    private $user_pass;

    protected function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:fresh');
        $this->seed('TestSeeder');
        $this->user = User::find(1);
        $this->user_pass = "password";
    }

    public function testInvalidLogin()
    {
        $response = $this->get(route('login'));
        $response->assertViewIs('sessions.create');
        $response = $this->followingRedirects()
                        ->post(route('login'), [
                            'email' => "",
                            'password' => "",
                        ]);
        $response->assertViewIs('sessions.create');
        $response->assertSeeText('Invalid email/password combination');
        $response = $this->get('/');
        $response->assertDontSeeText('Invalid email/password combination');
    }

    public function testValidLoginToLogout()
    {
        $this->get(route("login"));
        $response = $this->followingRedirects()
                        ->post(route("login"), [
                            'email'    => $this->user->email,
                            'password' => $this->user_pass
                        ]);
        $this->assertTrue(Auth::check());
        $response->assertViewIs('users.show');
        $dom = $this->dom($response->content());
        $this->assertSame(0, $dom->filter('a:contains("Log in")')->count());
        $this->assertSame(route("logout"), $dom->filter('form[name="logoutform"]')->attr("action"));
        $this->assertSame(route("users.show", $this->user), $dom->filter('a:contains("Profile")')->attr("href"));
        $response = $this->followingRedirects()->delete(route("logout"));
        $this->assertFalse(Auth::check());
        $response->assertViewIs("static_pages.home");
        $response = $this->followingRedirects()->delete(route("logout"));
        $dom = $this->dom($response->content());
        $this->assertSame(route("login"), $dom->filter('a:contains("Log in")')->attr("href"));
        $this->assertSame(0, $dom->filter('form[name="logoutform"]')->count());
        $this->assertSame(0, $dom->filter('a:contains("Profile")')->count());
    }

    public function testLoginRemember()
    {
        $response = $this->followingRedirects()
                        ->post(route("login"), [
                            'email' => $this->user->email,
                            'password' => $this->user_pass,
                            'remember_me' => "1"
                        ]);
        $i = 0;
        foreach ($response->headers->getCookies() as $cookie) {
            if (preg_match('/^remember_web_\w+$/', $cookie->getName())) {
                $i++;
            }
        }
        $this->assertSame(1, $i);
    }

    public function testLoginNotRemember()
    {
        /*$response = $this->followingRedirects()
                        ->post(route("login"), [
                            'email' => $this->user->email,
                            'password' => $this->user_pass,
                            'remember_me' => "1"
                        ]);*/
        $response = $this->followingRedirects()->delete('/logout');
        $response = $this->followingRedirects()
                        ->post(route("login"), [
                            'email' => $this->user->email,
                            'password' => $this->user_pass,
                            'remember_me' => "0"
                        ]);
        $i = 0;
        foreach ($response->headers->getCookies() as $cookie) {
            if (preg_match('/^remember_web_\w+$/', $cookie->getName())) {
                $i++;
            }
        }
        $this->assertSame(0, $i);
    }
}
