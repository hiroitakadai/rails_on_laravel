<?php

namespace Tests\Feature;

use App\User;
use App\Relationship;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

class FollowingTest extends TestCase
{
    private $user;
    private $other_user;

    protected function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:refresh');
        $this->seed('TestSeeder');
        $this->user = User::find(1);
        $this->other_user = User::find(2);
        $this->be($this->user);
    }

    public function testFollowingPage()
    {
        $response = $this->get(route("following", $this->user->id));
        $this->assertNotEmpty($this->user->following());
        $response->assertSeeText((string) $this->user->following()->count());
        $dom = $this->dom($response->content());
        foreach ($this->user->following as $following) {
            $this->assertSame(route("users.show", $following->id), $dom->filter("a:contains(\"{$following->name}\")")->attr("href"));
        }
    }

    public function testFollowersPage()
    {
        $response = $this->get(route("followers", $this->user->id));
        $this->assertNotEmpty($this->user->followers());
        $response->assertSeeText((string) $this->user->followers()->count());
        $dom = $this->dom($response->content());
        foreach ($this->user->followers as $follower) {
            $this->assertSame(route("users.show", $follower->id), $dom->filter("a:contains(\"{$follower->name}\")")->attr("href"));
        }
    }

    public function testFollowUser()
    {
        $count = Relationship::all()->count();
        $this->post(route("relationships.store"), ["followed_id" => $this->other_user->id]);
        $this->assertEquals($count + 1, Relationship::all()->count());
    }

    public function testUnfollowUser()
    {
        $this->user->follow($this->other_user->id);
        $count = Relationship::all()->count();
        $this->delete(route("relationships.destroy", $this->other_user->id));
        $this->assertEquals($count - 1, Relationship::all()->count());
    }
}
