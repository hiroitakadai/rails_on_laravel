<?php

namespace Tests\Feature;

use Tests\TestCase;

class SiteLayoutTest extends TestCase
{
    public function testLayoutLinks()
    {
        $response = $this->get("/");
        $response->assertViewIs("static_pages.home");
        $dom = $this->dom($response->content());
        $this->assertSame(url("/"), $dom->filter('a:contains("sample app")')->attr("href"));
        $this->assertSame(url("/"), $dom->filter('a:contains("Home")')->attr("href"));
        $this->assertSame(route("help"), $dom->filter('a:contains("Help")')->attr("href"));
        $this->assertSame(route("about"), $dom->filter('a:contains("About")')->attr("href"));
        $this->assertSame(route("contact"), $dom->filter('a:contains("Contact")')->attr("href"));
        $response = $this->get("contact");
        $response->assertViewIs("static_pages.contact");
    }
}
