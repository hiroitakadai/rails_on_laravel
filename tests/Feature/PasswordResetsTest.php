<?php

namespace Tests\Feature;

use App\User;
use App\Mail\PasswordReset;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class PasswordResetsTest extends TestCase
{
    private $user;

    protected function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:fresh');
        $this->seed('TestSeeder');
        $this->user = User::find(1);
    }

    public function testPasswordResets()
    {
        Mail::fake();

        $response = $this->get(route("resets.create"));
        $response->assertViewIs("password_resets.create");
        $response = $this->post(route("resets.store"), ["email" => ""]);
        $response->assertSessionHas("message");
        $response->assertRedirect(route("resets.create"));
        $response = $this->post(route("resets.store"), ["email" => $this->user->email]);
        $this->assertNotEquals($this->user->reset_digest, User::find(1)->reset_digest);
        Mail::assertSent(PasswordReset::class, 1);
        $response->assertSessionHas("message");
        $response->assertRedirect("/");

        $reset_token = str_random(22);
        $this->user->update(["reset_digest" => bcrypt($reset_token)]);
        $response = $this->get(route("resets.edit", ["token" => $reset_token, "email" => ""]));
        $response->assertRedirect("/");
        $this->user->update(["activated" => false]);
        $response = $this->get(route("resets.edit", ["token" => $reset_token, "email" => $this->user->email]));
        $response->assertRedirect("/");
        $this->user->update(["activated" => true]);
        $response = $this->get(route("resets.edit", ["token" => "wrong token", "email" => $this->user->email]));
        $response->assertRedirect("/");
        $response = $this->followingRedirects()
                        ->get(route("resets.edit", ["token" => $reset_token, "email" => $this->user->email]));
        $response->assertViewIs("password_resets.edit");
        $dom = $this->dom($response->content());
        $this->assertSame(1, $dom->filter("input[name=email][type=hidden][value=\"{$this->user->email}\"]")->count());
        $response = $this->followingRedirects()
                        ->patch(route("resets.update", $reset_token), [
                            "email" => $this->user->email,
                            "password" => "foobaz",
                            "password_confirmation" => "barquux"
                        ]);
        $this->assertSame(1, $this->dom($response->content())->filter("div#error_explanation")->count());
        $response = $this->followingRedirects()
                        ->patch(route("resets.update", $reset_token), [
                            "email" => $this->user->email,
                            "password" => "",
                            "password_confirmation" => ""
                        ]);
        $this->assertSame(1, $this->dom($response->content())->filter("div#error_explanation")->count());
        $response = $this->followingRedirects()
                        ->patch(route("resets.update", $reset_token), [
                            "email" => $this->user->email,
                            "password" => "foobaz",
                            "password_confirmation" => "foobaz"
                        ]);
        $this->assertTrue(Auth::check());
        $response->assertSeeText("Password has been reset.");
        $response->assertViewIs("users.show");
    }
}
