<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

class UsersProfileTest extends TestCase
{
    private $user;

    protected function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:fresh');
        $this->seed('TestSeeder');
        $this->user = User::find(1);
    }

    public function testProfileDisplay()
    {
        $response = $this->get(route("users.show", $this->user->id));
        $response->assertViewIs("users.show");
        $dom = $this->dom($response->content());
        $this->assertSame(full_title($this->user->name), $dom->filter("title")->text());
        $this->assertRegExp("/".$this->user->name."/", $dom->filter('h1')->text());
        $this->assertSame(1, $dom->filter("h1>img.gravatar")->count());
        $response->assertSeeText((string) $this->user->microposts()->count());
        $this->assertSame(1, $dom->filter("ul.pagination")->count());
        foreach ($this->user->microposts()->paginate(30) as $micropost) {
            $response->assertSeeText($micropost->content);
        }
    }
}
