<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

class UsersIndexTest extends TestCase
{
    private $admin;
    private $non_admin;

    protected function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:fresh');
        $this->seed('TestSeeder');
        $this->admin = User::find(1);
        $this->non_admin = User::find(2);
    }

    public function testIndexAsAdmin()
    {
        $this->be($this->admin);
        $response = $this->get(route("users.index"));
        $response->assertViewIs("users.index");
        $dom = $this->dom($response->content());
        $this->assertSame(2, $dom->filter("ul.pagination")->count());
        foreach (User::paginate(30) as $user) {
            $this->assertSame(route("users.show", $user->id), $dom->filter("a:contains(\"{$user->name}\")")->attr("href"));
            if ($user != $this->admin) {
                $this->assertSame(route("users.destroy", $user->id), $dom->filter("form[name=deleteform{$user->id}]")->attr("action"));
            }
        }
        $count = User::all()->count();
        $this->delete(route("users.destroy", $this->non_admin->id));
        $this->assertSame($count - 1, User::all()->count());
    }

    public function testIndexAsNonadmin()
    {
        $this->be($this->non_admin);
        $response = $this->get(route("users.index"));
        $dom = $this->dom($response->content());
        $this->assertSame(0, $dom->filter('a:contains("delete")')->count());
    }
}
