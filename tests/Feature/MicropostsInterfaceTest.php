<?php

namespace Tests\Feature;

use App\User;
use App\Micropost;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class MicropostsInterfaceTest extends TestCase
{
    private $user;

    protected function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:fresh');
        $this->seed('TestSeeder');
        $this->user = User::find(1);
    }

    public function testMicropostInterface()
    {
        Storage::fake('design');

        $this->be($this->user);
        $response = $this->get("/");
        $this->assertSame(1, $this->dom($response->content())->filter("ul.pagination")->count());
        $this->assertSame(1, $this->dom($response->content())->filter("input[type=file]")->count());
        $count = Micropost::all()->count();
        $response = $this->followingRedirects()->post(route("microposts.store"), ["content" => " "]);
        $this->assertEquals($count, Micropost::all()->count());
        $this->assertSame(1, $this->dom($response->content())->filter("div#error_explanation")->count());
        $content = "This micropost really ties the room together";
        $picture = UploadedFile::fake()->image('design.jpg');
        $count = Micropost::all()->count();
        $response = $this->followingRedirects()
                        ->post(route("microposts.store"), [
                            "content" => $content,
                            "picture" => $picture
                        ]);
        $this->assertEquals($count + 1, Micropost::all()->count());
        $response->assertViewIs("static_pages.home");
        $response->assertSeeText($content);
        $this->assertGreaterThan(0, $this->dom($response->content())->filter("a:contains(\"delete\")")->count());
        $first_micropost = $this->user->microposts->first();
        $count = Micropost::all()->count();
        $response = $this->followingRedirects()->delete(route("microposts.destroy", $first_micropost->id));
        $this->assertEquals($count - 1, Micropost::all()->count());
        $response = $this->get(route("users.show", 2));
        $this->assertSame(0, $this->dom($response->content())->filter("a:contains(\"delete\")")->count());
    }

    public function testSidebarCount()
    {
        $this->be($this->user);
        $response = $this->get("/");
        $response->assertSeeText("{$this->user->microposts()->count()} microposts");
        $other_user = User::find(4);
        $this->be($other_user);
        $response = $this->get("/");
        $response->assertSeeText("0 micropost");
        $other_user->microposts()->create(["content" => "A micropost"]);
        $response = $this->get("/");
        $response->assertSeeText("A micropost");
    }
}
