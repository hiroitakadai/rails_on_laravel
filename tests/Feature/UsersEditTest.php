<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

class UsersEditTest extends TestCase
{
    private $user;
    private $user_pass;

    protected function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:fresh');
        $this->seed('TestSeeder');
        $this->user = User::find(1);
        $this->user_pass = "password";
    }

    public function testUnsuccessfulEdit()
    {
        $this->be($this->user);
        $response = $this->get(route("users.edit", $this->user->id));
        $response->assertViewIs("users.edit");
        $response = $this->followingRedirects()
                        ->patch(route("users.update", $this->user->id), [
                            "name" => " ",
                            "password" => "foo",
                            "password" => "bar"
                        ]);
        $response->assertViewIs("users.edit");
    }

    public function testSuccessfulEdit()
    {
        $this->get(route("users.edit", $this->user->id));
        $response = $this->followingRedirects()
                        ->post(route("login"), [
                            "email" => $this->user->email,
                            "password" => $this->user_pass
                        ]);
        $response->assertViewIs("users.edit");
        $name  = "Foo Bar";
        $email = "foo@bar.com";
        $response = $this->followingRedirects()
                        ->patch(route("users.update", $this->user->id), [
                            "name"    => $name,
                            "email"    => $email,
                            "password" => "",
                            "password_confirmation" =>""
                        ]);
        $response->assertSeeText("Profile updated");
        $response->assertViewIs("users.show");
        $response = $this->get(route("users.show", $this->user->id));
        $this->assertEquals($name, $response->original->user->name);
        $this->assertEquals($email, $response->original->user->email);
    }
}
