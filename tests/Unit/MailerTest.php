<?php

namespace Tests\Unit;

use App\Mail\AccountActivation;
use App\Mail\PasswordReset;
use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;

class MailerTest extends TestCase
{
    protected function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:fresh');
        $this->seed('TestSeeder');
    }

    public function testAccountActivation()
    {
        Mail::fake();
        
        $user = User::find(1);
        $activation_token = str_random(22);
        Mail::to($user)->send(new AccountActivation($user, $activation_token));
        Mail::assertSent(AccountActivation::class, function ($mail) use ($user, $activation_token) {
            $mail->build();
            $this->assertEquals("Account activation", $mail->subject);
            $this->assertTrue($mail->hasTo($user->email));
            $this->assertTrue($mail->hasFrom("noreply@example.com"));
            $this->assertEquals($user->name, $mail->user->name);
            $this->assertEquals($activation_token, $mail->activation_token);
            $this->assertEquals($user->email, $mail->user->email);
            return true;
        });
    }

    public function testPasswordReset()
    {
        Mail::fake();
        
        $user = User::find(1);
        $reset_token = str_random(22);
        Mail::to($user)->send(new PasswordReset($user, $reset_token));
        Mail::assertSent(PasswordReset::class, function ($mail) use ($user, $reset_token) {
            $mail->build();
            $this->assertEquals("Password reset", $mail->subject);
            $this->assertTrue($mail->hasTo($user->email));
            $this->assertTrue($mail->hasFrom("noreply@example.com"));
            $this->assertEquals($user->name, $mail->user->name);
            $this->assertEquals($reset_token, $mail->reset_token);
            $this->assertEquals($user->email, $mail->user->email);
            return true;
        });
    }
}
