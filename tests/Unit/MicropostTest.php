<?php

namespace Tests\Unit;

use App\Micropost;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

class MicropostTest extends TestCase
{
    protected function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:fresh');
        $this->seed('TestSeeder');
    }

    public function testMicropostOrder()
    {
        $this->assertEquals(Micropost::orderBy("created_at", "desc")->first(), Micropost::first());
    }
}
