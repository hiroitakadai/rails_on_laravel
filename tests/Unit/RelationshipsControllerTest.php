<?php

namespace Tests\Unit;

use App\Relationship;
use Tests\TestCase;

class RelationshipsControllerTest extends TestCase
{
    public function testRedirectCreatePage()
    {
        $count = Relationship::all()->count();
        $response = $this->post(route("relationships.create"));
        $this->assertEquals($count, Relationship::all()->count());
        $response->assertRedirect(route("login"));
    }

    public function testRedirectDestroyPage()
    {
        $count = Relationship::all()->count();
        $response = $this->delete(route("relationships.destory", 1));
        $this->assertEquals($count, Relationship::all()->count());
        $response->assertRedirect(route("login"));
    }
}
