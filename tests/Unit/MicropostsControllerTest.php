<?php

namespace Tests\Unit;

use App\User;
use App\Micropost;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

class MicropostsControllerTest extends TestCase
{
    private $micropost;

    protected function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:fresh');
        $this->seed('TestSeeder');
        $this->micropost = Micropost::find(1);
    }

    public function testRedirectCreate()
    {
        $count = Micropost::all()->count();
        $response = $this->post(route("microposts.store", ["content" => "Lorem ipsum"]));
        $this->assertEquals($count, Micropost::all()->count());
        $response->assertRedirect(route("login"));
    }

    public function testRedirectDestroy()
    {
        $count = Micropost::all()->count();
        $response = $this->delete(route("microposts.destroy", $this->micropost->id));
        $this->assertEquals($count, Micropost::all()->count());
        $response->assertRedirect(route("login"));
    }

    public function testRedirectDestroyWrongMicropost()
    {
        $this->be(User::find(1));
        $micropost = Micropost::find(5);
        $count = Micropost::all()->count();
        $response = $this->delete(route("microposts.destroy", $micropost->id));
        $this->assertEquals($count, Micropost::all()->count());
        $response->assertRedirect("/");
    }
}
