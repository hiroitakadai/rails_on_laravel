<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

class UserTest extends TestCase
{
    protected function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:fresh');
        $this->seed('TestSeeder');
    }

    public function testFollowAndUnfollow()
    {
        $user_1 = User::find(1);
        $user_2 = User::find(2);
        $this->assertFalse($user_1->isFollowing($user_2->id));
        $user_1->follow($user_2->id);
        $this->assertTrue($user_1->isFollowing($user_2->id));
        $this->assertEquals(1, $user_1->following()->where("followed_id", $user_2->id)->count());
        $user_1->unfollow($user_2->id);
        $this->assertFalse($user_1->isFollowing($user_2->id));
    }

    public function testStatusFeed()
    {
        $user_1 = User::find(1);
        $user_2 = User::find(2);
        $user_3 = User::find(3);
        foreach ($user_3->microposts as $micropost) {
            $this->assertTrue($user_1->feed()->get()->contains($micropost));
        }
        foreach ($user_1->microposts as $micropost) {
            $this->assertTrue($user_1->feed()->get()->contains($micropost));
        }
        foreach ($user_2->microposts as $micropost) {
            $this->assertFalse($user_1->feed()->get()->contains($micropost));
        }
    }
}
