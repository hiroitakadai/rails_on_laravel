<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

class UsersControllerTest extends TestCase
{
    private $user;
    private $other_user;

    protected function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:fresh');
        $this->seed('TestSeeder');
        $this->user = User::find(1);
        $this->other_user = User::find(2);
    }

    public function testRedirectIndexGuest()
    {
        $response = $this->get(route("users.index"));
        $response->assertRedirect(route("login"));
    }

    public function testGetCreate()
    {
        $responce = $this->get(route('signup'));
        $responce->assertStatus(200);
    }

    public function testRedirectEditWithGuest()
    {
        $response = $this->get(route("users.edit", $this->user->id));
        $response->assertSessionHas("message");
        $response->assertRedirect(route("login"));
    }

    public function testRedirectUpdateWithGuest()
    {
        $response = $this->patch(route("users.update", $this->user->id), [
                        "name" => $this->user->name,
                        "email" => $this->user->email
                    ]);
        $response->assertSessionHas("message");
        $response->assertRedirect(route("login"));
    }

    public function testEditAdminAttributeViaWeb()
    {
        $this->be($this->other_user);
        $this->assertSame(false, $this->other_user->admin);
        $this->patch(route("users.update", $this->other_user->id), [
                "name" => $this->other_user->name,
                "email" => $this->other_user->email,
                "admin" => true
        ]);
        $this->assertSame(false, User::find(2)->admin);
    }

    public function testRedirectEditWrongUser()
    {
        $this->be($this->other_user);
        $response = $this->get(route("users.edit", $this->user->id));
        $response->assertSessionMissing("message");
        $response->assertRedirect("/");
    }

    public function testRedirectUpdateWrongUser()
    {
        $this->be($this->other_user);
        $response = $this->patch(route("users.update", $this->user->id), [
            "name" => $this->user->name,
            "email" => $this->user->email
        ]);
        $response->assertSessionMissing("message");
        $response->assertRedirect("/");
    }

    public function testRedirectDestroyGuest()
    {
        $count = User::all()->count();
        $response = $this->delete(route("users.destroy", $this->user->id));
        $this->assertEquals($count, User::all()->count());
        $response->assertRedirect(route("login"));
    }

    public function testRedirectDestroyNonadmin()
    {
        $this->be($this->other_user);
        $count = User::all()->count();
        $response = $this->delete(route("users.destroy", $this->user->id));
        $this->assertEquals($count, User::all()->count());
        $response->assertRedirect("/");
    }

    public function testRedirectFollowing()
    {
        $response = $this->get(route("following", 1));
        $response->assertRedirect(route("login"));
    }

    public function testRedirectFollowers()
    {
        $response = $this->get(route("followers", 1));
        $response->assertRedirect(route("login"));
    }
}
