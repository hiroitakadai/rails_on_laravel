@extends('layouts.application')

@section('title', "All users")

@section('content')
<h1>All users</h1>

{{ $users->links() }}

<ul class="users">
    @foreach ($users as $user)
        @include('users.user')
    @endforeach
</ul>

{{ $users->links() }}

@endsection
