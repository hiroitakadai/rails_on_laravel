@extends('layouts.application')

@section('title', 'Sign up')

@section('content')
<h1>Sign up</h1>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        @include("users.form", ["user"=> new App\User, "options" => ['route' => 'signup']])
    </div>
</div>
@endsection
