@extends('layouts.application')

@section('title', 'Edit user')

@section('content')
<h1>Update your profile</h1>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        @include("users.form", ["options" => ['route' => ['users.update', $user->id], "method" => "patch"]])

        <div class="gravatar_edit">
            {!! gravatar_for($user) !!}
            <a href="http://gravatar.com/emails" target="_blank" rel="noopener">change</a>
        </div>
    </div>
</div>
@endsection
