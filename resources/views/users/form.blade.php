{{ Form::model($user, $options) }}
@include('shared.error_messages')
{{ Form::label('name') }}
{{ Form::text('name', $user->name, ["class" => "form-control"]) }}
{{ Form::label('email') }}
{{ Form::email('email', $user->email, ["class" => "form-control"]) }}
{{ Form::label('password') }}
{{ Form::password('password', ["class" => "form-control"]) }}
{{ Form::label('password_confirmation') }}
{{ Form::password('password_confirmation', ["class" => "form-control"]) }}

{{ Form::submit("Save changes", ["class" => "btn btn-primary"]) }}
{{ Form::close() }}
