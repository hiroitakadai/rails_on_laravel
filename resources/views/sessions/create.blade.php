@extends('layouts.application')

@section('title', 'Log in')

@section('content')
<h1>Log in</h1>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        {{ Form::open(['route' => 'login']) }}

        {{ Form::label('email') }}
        {{ Form::text('email', "", ["class" => "form-control"]) }}

        {{ Form::label('password') }}
        {{ Html::linkRoute("resets.create", "(forgot password)") }}
        {{ Form::password('password', ["class" => "form-control"]) }}

        <label class="checkbox inline">
            {{ Form::hidden("remember_me", "0") }}
            {{ Form::checkbox("remember_me", "1", null, ["id" => "session_remember_me"]) }}
            <span>Remember me on this computer</span>
        </label>

        {{ Form::submit("Log in", ["class" => "btn btn-primary"]) }}
        {{ Form::close() }}

        <p>New user? {{ Html::link(route("signup"), "Sign up now!") }}</p>

        <p>
          テストアカウントEmail ： Password<br>
          aaaaaa@test.test : aaaaaa<br>
          bbbbbb@test.test : bbbbbb<br>
          cccccc@test.test : cccccc<br>
        </p>
    </div>
</div>
@endsection
