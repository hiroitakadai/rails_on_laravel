@extends('layouts.application')

@section('title', 'Reset password')

@section('content')
<h1>Reset password</h1>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        {{ Form::model($user, ["route" => ["resets.update", $token], "method" => "patch"]) }}
        @include('shared.error_messages')
        {{ Form::hidden('email', $user->email) }}
        {{ Form::label('password') }}
        {{ Form::password('password', ["class" => "form-control"]) }}
        {{ Form::label('password_confirmation') }}
        {{ Form::password('password_confirmation', ["class" => "form-control"]) }}
        
        {{ Form::submit("Update password", ["class" => "btn btn-primary"]) }}
        {{ Form::close() }}
    </div>
</div>
@endsection
