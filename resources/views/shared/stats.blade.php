@php empty($user) && $user = Auth::user() @endphp
<div class="stats">
    <a href="{{ route("following", $user->id) }}">
        <strong id="following" class="stat">
            {{ $user->following()->count() }}
        </strong>
        following
    </a>
    <a href="{{ route("followers", $user->id) }}">
        <strong id="followers" class="stat">
            {{ $user->followers()->count() }}
        </strong>
        followers
    </a>
</div>
