<ol class="microposts">
    @foreach ($feed_items as $micropost)
        @include("microposts.micropost")
    @endforeach
</ol>
{{ $feed_items->links() }}
