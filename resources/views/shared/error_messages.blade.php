@if ($errors->any())
<div id="error_explanation">
    <div class="alert alert-danger">
        The form contains {{ $errors->count() . str_plural('error', $errors->count()) }}.
    </div>
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul>
</div>
@endif
