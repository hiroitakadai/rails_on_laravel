{{ Form::open(["route" => "microposts.store", 'files' => true]) }}
@include('shared.error_messages')
<div class="field">
    {{ Form::textarea('content', null, ["placeholder" => "Compose new micropost..."]) }}
</div>
{{ Form::submit("Post", ["class" => "btn btn-primary"]) }}
<span class="picture">
    {{ Form::file("picture", ["id" => "micropost_picture"]) }}
</span>
{{ Form::close() }}

<script type="text/javascript">
    $('#micropost_picture').bind('change', function() {
      var size_in_megabytes = this.files[0].size/1024/1024;
      if (size_in_megabytes > 5) {
        alert('Maximum file size is 5MB. Please choose a smaller file.');
      }
    });
</script>
