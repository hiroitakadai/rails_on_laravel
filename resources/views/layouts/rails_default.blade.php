<meta name="csrf-token" content="{{ csrf_token() }}">
{{ Html::script(mix('js/app.js'), ["media" => "all", "data-turbolinks-track" => "reload"]) }}
{{ Html::style(mix('css/app.css'), ["media" => "all", "data-turbolinks-track" => "reload"]) }}
