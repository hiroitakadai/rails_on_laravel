<!DOCTYPE html>
<html>
    <head>
        <title>{{ full_title($__env->yieldContent('title')) }}</title>
        @include('layouts.rails_default')
        @include('layouts.shim')
    </head>
    <body>
        @include('layouts.header')
        <div class="container">
            @if(session('message'))
                @foreach (session('message') as $message_type => $message)
                    <div class="alert alert-{{ $message_type }}">{{ $message }}</div>
                @endforeach
            @endif
            @yield('content')
            @include('layouts.footer')
        </div>
    </body>
</html>
