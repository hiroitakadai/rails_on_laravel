<header class="navbar navbar-fixed-top navbar-inverse">
    <div class="container">
        {{ Html::link("/", "sample app", ["id" => "logo"]) }}
        <nav>
            <ul class="nav navbar-nav navbar-right">
                <li>{{ Html::link("/", "Home") }}</li>
                <li>{{ Html::linkRoute("help", "Help") }}</li>
                @if (Auth::check())
                    <li>{{ Html::linkRoute("users.index", "Users") }}</li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Account <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li>{{ Html::linkRoute("users.show", "Profile", [Auth::id()]) }}</li>
                            <li>{{ Html::linkRoute("users.edit", "Settings", [Auth::id()]) }}</li>
                            <li class="divider"></li>
                            <li>
                                <a href="javascript:document.logoutform.submit()">Log out</a>
                                {{ Form::open(["route" => "logout", "method" => "delete", "name" => "logoutform"]) }}
                                {{ Form::close() }}
                            </li>
                        </ul>
                    </li>
                @else
                    <li>{{ Html::linkRoute("login", "Log in") }}</li>
                @endif
            </ul>
        </nav>
    </div>
</header>
