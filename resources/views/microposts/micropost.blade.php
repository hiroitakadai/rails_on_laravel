<li id="micropost-{{ $micropost->id }}">
    <a href="{{ route("users.show", $micropost->user_id) }}">{!! gravatar_for($micropost->user, ["size" => 50]) !!}</a>
    <span class="user">{{ Html::linkRoute("users.show", $micropost->user->name, $micropost->user_id) }}</span>
    <span class="content">
        {{ $micropost->content }}
        @if ($micropost->picture)
            <img src="{{ Storage::url($micropost->picture) }}">
        @endif
    </span>
    <span class="timestamp">
        Posted {{ time_ago_in_words($micropost->created_at) }} ago.
        @if (Auth::id() == $micropost->user_id)
            <a href="javascript:if(window.confirm('Yes Sure?')){document.deleteform{{ $micropost->id }}.submit()}">delete</a>
            {{ Form::open(["route" => ["microposts.destroy", $micropost->id], "method" => "delete", "name" => "deleteform{$micropost->id}"]) }}
            {{ Form::close() }}
        @endif
    </span>
</li>
