@extends('layouts.application')

@section('content')
@if (Auth::check())
    <div class="row">
        <aside class="col-md-4">
            <section class="user_info">
                @include("shared.user_info")
            </section>
            <section class="stats">
                @include ("shared.stats")
            </section>
            <section class="micropost_form">
                @include("shared.micropost_form")
            </section>
        </aside>
        <div class="col-md-8">
                <h3>Micropost Feed</h3>
                @includeWhen($feed_items, "shared.feed")
        </div>
    </div>
@else
    <div class="center jumbotron">
        <h1>Welcome to the Sample App</h1>

        <h2>
            This is the home page for the
            <a href="https://railstutorial.jp/">Ruby on Rails Tutorial</a>
            sample application.
        </h2>

        {{ Html::linkRoute('signup', "Sign up now!", [], ["class" => "btn btn-lg btn-primary"]) }}
    </div>

    <a href="http://rubyonrails.org/">{{ Html::image("img/rails.png", "Rails logo") }}</a>
@endif
@endsection
